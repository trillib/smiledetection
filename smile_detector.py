import cv2
import numpy as np
import face_recognition
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing.image import img_to_array


class SmileDetector:

    def __init__(self):
        self.classifier = load_model('./model_from_scratch.h5')

    def recognize_faces_from_video(self):
        """
        Hauptmethode der Klasse.
        Startet die Facerecognition sowie den Classifier.
        Die Classification wird nur jedes 2te Frame gemacht, um Ressource zu sparen.
        Es hat sich gezeigt, dass auf dem Videostream eher zu häufig "Not Smiling"
        anstatt "Smiling" erkannt wurde, deshalb wurde der Threshold für Smiling auf 0.3
        gesetzt.
        """
        video_capture = cv2.VideoCapture(0)
        predicted_class = 0
        threshold = 0.3
        index = 0
        predict_ratio = 2
        while True:
            frame, smaller_frame = self._get_video_frames(
                video_capture)
            face_images, face_locations = self._get_face_images(
                smaller_frame)
            if index == predict_ratio:
                for face_image in face_images:
                    converted_image = self._convert_face_image(face_image)
                    predicted_class = 1 if self.classifier.predict(
                        converted_image)[0][1] > threshold else 0
                index = 0
            else:
                index += 1

            self._display_results(face_locations, predicted_class, frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

        # Release handle to the webcam
        video_capture.release()
        cv2.destroyAllWindows()

    def _get_video_frames(self, video_capture):
        """
        Liefert 2 Bilder zurück: Das grosse Bild zum Anzeigen, sowie
        ein Bild mit Grösse x0.25, welches besser verarbeitet werden kann.
        """
        _, frame = video_capture.read()
        small_frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)
        return frame, small_frame

    def _get_face_images(self, frame):
        """
        Methode welche mithilfe von der Bibliothek "face_recognition"
        die Gesichter liefert.
        """
        faces_img = []
        face_locations = face_recognition.face_locations(frame)
        for face_location in face_locations:
            top, right, bottom, left = face_location
            cropped_face = frame[top:bottom, left:right]
            faces_img.append(cropped_face)
        return faces_img, face_locations

    def _convert_face_image(self, face_image):
        """
        Konvertiert ein Bild in das gewünschte Format
        Das Bild wird auf die Grösse 28 gebracht und anschliessend in ein
        Array konvertiert.
        """
        converted_face_image = cv2.cvtColor(
            face_image, cv2.COLOR_BGR2GRAY)
        converted_face_image = cv2.equalizeHist(converted_face_image)
        converted_face_image = cv2.resize(converted_face_image, (28, 28))
        converted_face_image = img_to_array(converted_face_image)
        converted_face_image = [converted_face_image]
        converted_face_image = np.array(
            converted_face_image, dtype="float") / 255.0
        return converted_face_image

    def _display_results(self, face_locations, label, frame):
        """
        Zeigt das Frame, sowie einen Rahmen um das Gesicht sowie
        die Klasse an.
        """
        label_names = ["Not Smiling", "Smiling"]
        for (top, right, bottom, left) in face_locations:
            # Positionen müssen noch hochskaliert werden, da sie auf dem Frame mit grösse 0.25 erstellt wurden.
            top *= 4
            right *= 4
            bottom *= 4
            left *= 4

            # Box für Gesicht
            cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)

            # Label unter der Box
            cv2.rectangle(frame, (left, bottom - 35),
                          (right, bottom), (0, 0, 255), cv2.FILLED)
            font = cv2.FONT_HERSHEY_DUPLEX
            label_name = label_names[label]
            cv2.putText(frame, label_name, (left + 6, bottom - 6),
                        font, 1.0, (255, 255, 255), 1)

        # Resultat anzeigen
        cv2.imshow('Smile Detection', frame)


if __name__ == "__main__":
    SmileDetector().recognize_faces_from_video()
